const mongoose = require('mongoose');

const preguntaSchema = mongoose.Schema({
    category: String,
    type: String,
    difficulty: String,
    question: String,
    correct_answer: String,
    incorrect_answers: Array,
    fecha_id: String
},
{
    versionKey: false 
}
);

module.exports = mongoose.model('Pregunta', preguntaSchema);