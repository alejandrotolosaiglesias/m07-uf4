const mongoose = require('mongoose');
const Pregunta = require('../models/db/preguntaModel');
const Busqueda = require('../models/db/busquedaModel');
const crudRepository = require('../database/crudRepository');
const axios = require('axios');

//https://opentdb.com/api.php?amount=1
//FUNCIONA
module.exports.getpregunta = async function() {
    const responseObj = { status: false };
    try {
        var d = new Date();
        const fecha =d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
        const busqueda = new Busqueda({"fecha_hora": fecha});
        const pruebaaa = await crudRepository.save(busqueda);
        const id_fecha=pruebaaa.result._id;
        console.log (id_fecha);
 


        const {data:response} = await axios.get("https://opentdb.com/api.php?amount=1")
        var obj = JSON.stringify(response.results).slice(1, -2);
        const fin= obj+',"fecha_id":"'+id_fecha+'"}';
        const final = JSON.parse(fin)

        const pregunta = new Pregunta(final);
        const responseFromDatabase = await crudRepository.save(pregunta);


        //SE TENDRAN Q COMPROBAR LOS DOS INSERTS
        if (responseFromDatabase.status) {
            responseObj.status = true;
            responseObj.result = responseFromDatabase.result;
        }
    } catch (error){
        console.log('ERROR-busquedaService-create: ', error);
    }
    return responseObj;
}