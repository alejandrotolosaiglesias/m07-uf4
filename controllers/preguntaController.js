const preguntaService = require('../services/preguntaService');

//FPARA HACER EL SELECT DE LA API. NO FUNCIONA
module.exports.selectapi = async function(req, res) {
    const responseObj = { status: 500, message: 'Internal server error' };
    try {
        const responseFromService = await preguntaService.getpregunta();
        if (responseFromService.status) {
            if (responseFromService.result) {
                responseObj.body = responseFromService.result;
                responseObj.message = 'Pregunta fetched successfully';
                responseObj.status = 200;
            } else {
                responseObj.message = 'Pregunta not found';
                responseObj.status = 404;
            }
        }
    } catch(error) {
        console.log('ERROR-preguntaController-selectapi: ', error);
    }
    return res.status(responseObj.status).send(responseObj);
}
